# You can put files here to add functionality separated per file, which
# will be ignored by git.
# Files on the custom/ directory will be automatically loaded by the init
# script, in alphabetical order.

# For example: add yourself some shortcuts to projects you often work on.
#
# brainstormr=~/Projects/development/planetargon/brainstormr
# cd $brainstormr
#
# don't die if no glob
setopt +o nomatch
# don't autocorrect just more hassle
unsetopt correctall
# set editor to nvim if it exists
if [ -e "/usr/bin/nvim" ]; then
    export EDITOR=/usr/bin/nvim
fi
# other stuff not plugins
source ~/sharedconfig/omz/custom/plugins/auto-ls/auto-ls.zsh
source ~/sharedconfig/omz/custom/plugins/enhancd/init.sh
# add FZF
source ~/sharedconfig/.fzf.zsh
export FZF_DEFAULT_OPTS='--height 40% --layout=reverse --border'
export FZF_DEFAULT_COMMAND='ag --hidden --ignore .git -g ""'
# add zFZF
source ~/sharedconfig/fz.sh
# add FZF aliases for common stuff
alias lsf='ls | fzf'
alias laf='la | fzf'
# Bind up and down arrow to history search
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
# ls colors
eval $( dircolors -b $HOME/sharedconfig/LS_COLORS )
# power level over 9,000!
# add the anaconda segment to powerlevel9k
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir anaconda vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator background_jobs time)
# make the pl9k not so big
POWERLEVEL9K_HOME_ICON=''
POWERLEVEL9K_HOME_SUB_ICON=''
POWERLEVEL9K_FOLDER_ICON=''
# for vcs hide everything but branch
POWERLEVEL9K_HIDE_BRANCH_ICON=true
POWERLEVEL9K_VCS_SHOW_SUBMODULE_DIRTY=false
POWERLEVEL9K_VCS_HIDE_TAGS=true
POWERLEVEL9K_VCS_GIT_HOOKS=()
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_SHORTEN_DIR_LENGTH=5
POWERLEVEL9K_SHORTEN_DELIMITER="*"
POWERLEVEL9K_SHORTEN_STRATEGY="truncate_middle"
export PATH=$HOME/anaconda3/bin:$PATH
