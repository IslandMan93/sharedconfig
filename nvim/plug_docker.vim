" Deoplete for Docker
Plug 'zchee/deoplete-docker'
" Syntax https://stackoverflow.com/a/28117335/3403018
" Set the filetype based on the file's extension, but only if
" 'filetype' has not already been set
au BufRead,BufNewFile Dockerfile* setfiletype dockerfile

