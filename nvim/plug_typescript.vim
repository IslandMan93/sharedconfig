" IMO the typescript language servers don't provide what I need
" theia-ide is better than sourcegraph's but mhartington/nvim-typescript provides
" a more descriptive view of the function args
"Plug 'HerringtonDarkholme/yats.vim'
"Plug 'mhartington/nvim-typescript', {'do': './install.sh'}
" You'll need to have node and npm installed then do
" npm install -g neovim
" Finally :UpdateRemotePlugins

" Syntax tsx should be handled by ts
autocmd BufNewFile,BufRead *.tsx set syntax=typescript
