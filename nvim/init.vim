" Base Config
source ~/sharedconfig/nvim/base_config.vim
" Plugins
call plug#begin('~/.vim/plugged')
Plug 'autozimu/LanguageClient-neovim', { 'branch': 'next', 'do': 'bash install.sh'}
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'Shougo/echodoc.vim'
Plug 'vim-airline/vim-airline'
Plug 'AndrewRadev/linediff.vim'
" LanguageClient doesn't provide syntax polyglot does a good job on most files
Plug 'sheerun/vim-polyglot'

""" Python
" Async autocompletion with jedi, better than the palantir language server
Plug 'zchee/deoplete-jedi'
" Python goto definition, refactor, renaming and usages. Again just slightly better than the language server
Plug 'davidhalter/jedi-vim'
" Polyglot's python highlighting isn't as good as this one
" Adds nice things like selected, self, arguments used
Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}
Plug 'google/yapf', { 'rtp': 'plugins/vim', 'for': 'python' }
""" End Python

" Multi-entry selection UI.
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
" Move to anywhere on the screen quickly
Plug 'easymotion/vim-easymotion'

""" Additional stuff that makes vim better but not needed
" Wrap text in various closing/opening chars 
Plug 'tpope/vim-surround'
" Redo last command with plugin support
Plug 'tpope/vim-repeat'
" Git stuff in vim
Plug 'tpope/vim-fugitive'
" Highlight all incsearch hits and more goodies
Plug 'haya14busa/incsearch.vim'
" Autosave
Plug '907th/vim-auto-save'
" Git changes in the sign column
Plug 'airblade/vim-gitgutter'
" Highlight yanks
Plug 'machakann/vim-highlightedyank'
" Commenting support
Plug 'scrooloose/nerdcommenter'
" Undo tree
Plug 'sjl/gundo.vim' 

""" Theme One Dark
Plug 'joshdick/onedark.vim'
""" End Theme

""" Languages config
" Typescript
source ~/sharedconfig/nvim/plug_typescript.vim
" Docker
source ~/sharedconfig/nvim/plug_docker.vim

call plug#end()

set hidden

" easymotion Config
let g:EasyMotion_keys='tnseriaodhplvmfuc,wyx.qtnseri' " colemak keys
" remap s to easy motion
nmap s <Plug>(easymotion-s2)
" easymotion f&t
map  f <Plug>(easymotion-bd-fl)

" Don't auto comment the next line and don't let anyone overwrite this setting
" https://stackoverflow.com/a/8748154
set formatoptions-=cro
autocmd BufReadPost * setlocal formatoptions-=cro

" autosave by default
let g:auto_save = 1

" Git stuff
set diffopt+=vertical " Always open git diff in vertical

" Better incsearch
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)
let g:incsearch#auto_nohlsearch = 1
map n  <Plug>(incsearch-nohl-n)
map N  <Plug>(incsearch-nohl-N)
map *  <Plug>(incsearch-nohl-*)
map #  <Plug>(incsearch-nohl-#)
map g* <Plug>(incsearch-nohl-g*)
map g# <Plug>(incsearch-nohl-g#)

" Deoplete and python language server
let g:deoplete#enable_at_startup = 1
let g:echodoc#enable_at_startup = 1
call deoplete#custom#option({
\ 'max_list': 15,
\ 'ignore_case': v:true
\ })
" Airline shows mode
set noshowmode
set shortmess+=c " Don't show match x of y
" Language server config
let g:LanguageClient_autoStart = 1
let g:LanguageClient_serverCommands = { 
	\ 'javascript': ['/usr/local/bin/javascript-typescript-stdio']}
" Always show the gutter so errors don't change the screen
set signcolumn=yes
" Log language server to file, helps in debugging
"let g:LanguageClient_loggingLevel = 'INFO'
"let g:LanguageClient_loggingFile = expand('~/.local/share/nvim/LanguageClient.log')
"let g:LanguageClient_serverStderr = expand('~/.local/share/nvim/LanguageServer.log')

" Auto select the first completion https://vi.stackexchange.com/a/15403
set completeopt=menu,noinsert

nnoremap <F5> :call LanguageClient_contextMenu()<CR>
" Or map each action separately
nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> gv :call LanguageClient#textDocument_definition({'gotoCmd':':rightbelow vsplit'})
nnoremap <silent> gh :call LanguageClient#textDocument_definition({'gotoCmd':'split'})<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>
" TODO: map typescript actions

""" Python Language Setup
" Autocomplete is handled by jedi-deoplete
let g:jedi#completions_enabled = 0
let g:jedi#use_splits_not_buffers = "right"
let g:jedi#auto_vim_configuration = 0
" Theme syntax highlighting
" Python is handled in a separate file
let g:polyglot_disabled = ['python']
source ~/sharedconfig/nvim/python_highlights_onedark.vim
""" End Python Language Setup

syntax on
colorscheme onedark

""" Additional plugins Setup
" My personal favorite: vim-surround on _ with entered fn
" example (cursor is *): *np.prod(test) ysW_int<CR> -> int(np.prod(test))
let g:surround_95 = "\1prepend: \1(\r)"
" The opposite of above, wraps and appends
let g:surround_45 = "(\r)\1append: \1"
