# install neovim
sudo apt-get install -y software-properties-common
sudo add-apt-repository ppa:neovim-ppa/stable
sudo apt-get update
sudo apt-get install -y neovim

# install vim plug
sudo apt install -y curl
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
# install silver searcher
sudo apt install -y silversearcher-ag
mkdir ~/.config/nvim
ln -s ~/sharedconfig/nvim/init.vim ~/.config/nvim/init.vim

# start nvim
# :PlugInstall

# js/ts server IMO isn't ready for fulltime use yet
# sudo npm install -g javascript-typescript-langserver
# instead nvim-typescript works very well (requires node neovim plugin)
sudo npm install -g neovim
# run after doing PlugInstall and UpdateRemotePlugins
# bash ~/.vim/plugged/nvim-typescript/install.sh
