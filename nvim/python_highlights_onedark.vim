function! SynGroup()                                                            
    let l:s = synID(line('.'), col('.'), 1)                                       
    echo synIDattr(l:s, 'name') . ' -> ' . synIDattr(synIDtrans(l:s), 'name')
endfun
map <F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>
map <F9> :call SynGroup()<CR>
" IMO cyan is not different enough from blue (39) in 256, change it to 37 (instead of 38)
let g:onedark_color_overrides = {
    \ "cyan": {'gui': '#56B6C2', 'cterm': '37', 'cterm16': '4'}}

" Disable some highlighting that's not needed
let g:semshi#excluded_hl_groups = ['local', 'imported', 'global', 'free', 'parameter']

function PythonExtraHighlighting()
    " Clear python function syntax it interferes with the below
    syntax clear pythonFunction
    " This interferes weirdly with the class def 
    syntax clear pythonAttribute
    syntax clear pythonStatement
    " Matches .<word>()
    " syntax match pythonFunctionCall '\(\.\)\@<=\(\w\+\)\((\)\@='
    " Matches <word>(
    syntax match pythonFunctionCall '\v\zs\w+\ze\(' 
    " Matches keyword arguments 
    syntax match pythonKeywordArgs '\v\zs\w+\ze\='
    " Matches function definition
    syntax region pythonFnDef start='\v^\s*def .*\(' end='\v\)(:| )' contains=pythonStatement,pythonFnDefName,pythonFnDefBuiltinName,pythonFnArguments keepend 
    syntax match pythonFnDefName '\v(\w+)(\()@=' contained
    syntax match pythonFnDefBuiltinName '\v__(\w+)__(\()@=' contained
    syntax region pythonFnArguments start='(' end='):| ' contained containedin=pythonFnDef contains=pythonString,pythonFnArg keepend
    " matches the words in the () of a function
    syntax match pythonFnArg '\v\w+' contained containedin=pythonFnArguments

    " Matches class with no parens
    syntax region pythonClassDefNoParen start='\v^\s*class ' end=':' contains=pythonStatement,pythonClassDefNoParenName keepend
    syntax match pythonClassDefNoParenName '\v\zs\w+\ze:' contained
    " Matches class def name
    syntax region pythonClassDef start='\v^\s*class .*\(' end='\v\)(:| )' contains=pythonStatement,pythonClassDefName,pythonClassDefInParens keepend 
    " Matches the class name 'word before ('
    syntax match pythonClassDefName '\v\zs\w+\ze\(' contained
    " Matches all inherited class names inside (<name>, <name2>)
    syntax region pythonClassDefInParens start='(' end='):' contained containedin=pythonClassDef contains=pythonClassDefBaseClass keepend 
    syntax match pythonClassDefBaseClass '\v[^.,:\(\)]*' contained containedin=pythonClassDefInParens
    
    " Add back in pythonStatement keywords
    syntax keyword pythonStatement break continue del exec return pass raise global nonlocal assert yield lambda with as
    " Contained in pythonClassDef and pythonFnDef
    syntax keyword pythonStatement class def contained

    " Trailing whitespace
    syntax match trailingWhitespace '\s\+$'

    " Matches __<word>__( and overrides everything else
    match pythonBuiltinFunctionCall '\v(__\w+__)(\()@=' 
endfun



if (has("autocmd"))
    augroup colorextend
        autocmd!
        
        " Get colors from onedark theme
        let s:colors = onedark#GetColors()

        """ Custom syntax define colors
        " highlight trailing whitespace
        autocmd FileType python call onedark#set_highlight("trailingWhitespace", { "bg": s:colors.yellow })
        " function calls -> blue
        autocmd FileType python call onedark#set_highlight("pythonFunctionCall", { "fg": s:colors.blue })
        " builtin function calls -> cyan
        autocmd FileType python call onedark#set_highlight("pythonBuiltinFunctionCall", { "fg": s:colors.cyan })
        " function definition -> blue
        autocmd FileType python call onedark#set_highlight("pythonFnDefName", { "fg": s:colors.blue })
        " built in function definition -> cyan
        autocmd FileType python call onedark#set_highlight("pythonFnDefBuiltinName", { "fg": s:colors.cyan })
        " function arguments dark yellow
        autocmd FileType python call onedark#set_highlight("pythonFnArg", { "fg": s:colors.dark_yellow })
        " keyword arguments -> dark_yellow
        autocmd FileType python call onedark#set_highlight("pythonKeywordArgs", { "fg": s:colors.dark_yellow })
        " Class definition -> yellow
        autocmd FileType python call onedark#set_highlight("pythonClassDefName", { "fg": s:colors.yellow })
        autocmd FileType python call onedark#set_highlight("pythonClassDefNoParenName", { "fg": s:colors.yellow })
        " Class base class -> greed
        autocmd FileType python call onedark#set_highlight("pythonClassDefBaseClass", { "fg": s:colors.green })
        """ End Custom Syntax

        """ semshi defined highlights
        " self -> red
        autocmd FileType python call onedark#set_highlight("semshiSelf", { "fg": s:colors.red })
        " args -> dark_yellow
        autocmd FileType python call onedark#set_highlight("semshiParameter", { "fg": s:colors.white })
        autocmd FileType python call onedark#set_highlight("semshiParameterUnused", { "fg": s:colors.dark_yellow, "cterm": "bold" })
        " unused vars -> yellow + bold
        autocmd FileType python call onedark#set_highlight("semshiUnresolved", { "fg": s:colors.yellow, "cterm": "bold" })
        " builtin -> cyan
        autocmd FileType python call onedark#set_highlight("semshiBuiltin", { "fg": s:colors.cyan })
        " don't bold or color imports
        autocmd FileType python call onedark#set_highlight("semshiImported", { "fg": s:colors.white })
        " highlight with a light gray
        autocmd FileType python call onedark#set_highlight("semshiSelected", { "bg": s:colors.visual_grey })
        " Attributes -> blue
        autocmd FileType python call onedark#set_highlight("semshiAttribute", { "fg": s:colors.white})
        " Globals don't color
        autocmd FileType python call onedark#set_highlight("semshiGlobal", { "fg": s:colors.white})
        " Free don't color
        autocmd FileType python call onedark#set_highlight("semshiFree", { "fg": s:colors.white})
        """ end semshi

        """ predefined syntax coloring
        " Python decorator -> cyan
        autocmd ColorScheme * call onedark#set_highlight("pythonDecorator", { "fg": s:colors.cyan})
        autocmd ColorScheme * call onedark#set_highlight("pythonDecoratorName", { "fg": s:colors.cyan})
        " Built in functions -> cyan
        autocmd ColorScheme * call onedark#set_highlight("pythonBuiltinFunc", { "fg": s:colors.cyan})
        autocmd ColorScheme * call onedark#set_highlight("pythonBuiltin", { "fg": s:colors.cyan})
        " Built in objects -> cyan
        autocmd ColorScheme * call onedark#set_highlight("pythonBuiltinObj", { "fg": s:colors.cyan})
        " Python exceptions -> cyan
        autocmd ColorScheme * call onedark#set_highlight("pythonExClass", { "fg": s:colors.cyan})
        """ end predefined
    augroup END
    " extra syntax goodies
    augroup Python
        autocmd!
        autocmd BufNewFile,BufReadPost *.py call PythonExtraHighlighting()
    augroup end
endif
