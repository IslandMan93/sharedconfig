# Setup fzf
# ---------
if [[ ! "$PATH" == *$HOME/sharedconfig/fzf/bin* ]]; then
  export PATH="$PATH:$HOME/sharedconfig/fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "$HOME/sharedconfig/fzf/shell/completion.zsh" 2> /dev/null

# Key bindings
# ------------
source "$HOME/sharedconfig/fzf/shell/key-bindings.zsh"


