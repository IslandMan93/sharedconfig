### OPTIONALS
# fix volume
sudo echo "ctl.!default { type pulse }" > /etc/asound.conf

# auto lock screen
sudo apt install -y xautolock
sudo chmod +x ~/sharedconfig/twm/lock.sh

# playerctl
cd /tmp
wget https://github.com/acrisci/playerctl/releases/download/v2.0.2/playerctl-2.0.2_amd64.deb
sudo dpkg -i playerctl-2.0.2_amd64.deb
cd ~

# background
sudo apt install -y feh

# save your eyes
sudo apt install -y redshift

# screenshot util
sudo apt install -y flameshot
# From an ubuntu upgrade I had to enable this app to run as sudo IDK why
# sudo visudo
# add username ALL = NOPASSWD: /usr/bin/flameshot
# then also change the bspwm/sxhkdrc flameshot gui -> sudo flameshot gui
