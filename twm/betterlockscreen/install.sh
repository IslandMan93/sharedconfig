# for i3 better lock screen
# i3 color
sudo apt install libpam-dev xautolock
sudo apt install liblibev-dev xcb libxcb-xkb1 libxkbcommon-x11-dev libxcb-xinerama0 libxcb-xinerama0-dev libxcb-randr0 libxcb-composite0 libxcb-composite0-dev
cd /tmp
mkdir i3color
cd i3color
git clone --branch 2.11-c https://github.com/PandorasFox/i3lock-color/
cd i3lock-color
git tag -f "git-$(git rev-parse --short HEAD)"
autoreconf -i && ./configure && sudo make install

cd ~/sharedconfig/twm/betterlockscreen
./betterlockscreen.sh -u ~/sharedconfig/twm/betterlockscreen/pc-master-race-uhd-8k-wallpaper.jpg
