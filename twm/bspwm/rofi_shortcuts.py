import subprocess

LINE_LENGTH = 93  # default on my 1080p
ROFI_SEPARATOR = ')'
SHOW_COMMAND = False
with open('/home/ben/sharedconfig/twm/bspwm/sxhkdrc', 'r') as in_file:
    lines = in_file.readlines()

last_line_comment = None
current_binding = None

commands = []
for l in lines:
    if l[0] == '#' or l[0].strip(' ') == '':  # comment or empty
        last_line_comment = l
    elif l[0] == '\t' or l[0] == ' ':  # command
        # TODO: multiline commands end with \
        commands.append((last_line_comment, current_binding, l))
        last_line_comment = None
        current_binding = None
    else:  # binding
        current_binding = l

str_to_rofi = ''
for c in commands:
    # failed lines
    if c[1] is not None and c[2] is not None:
        # add comment line
        if c[0] is not None:
            comment = '{}'.format(c[0].strip('# \n'))
            keybinding = '[{}] '.format(c[1].strip(' \n'))
            space = ' ' * (LINE_LENGTH - len(comment) - len(keybinding))
            str_to_rofi += '{comment}{space}{keybinding}'.format(comment=c[0].strip('# \n'), space=space, keybinding=keybinding)
        else:
            str_to_rofi += '{}'.format(c[1].strip(' \n'))
        str_to_rofi += '\n{}{sep}'.format(c[2], sep=ROFI_SEPARATOR)
output = subprocess.run('echo "{}" | rofi -dmenu -eh {} -sep \'{sep}\''.format(str_to_rofi, 2 if SHOW_COMMAND else 1, sep=ROFI_SEPARATOR), shell=True, stdout=subprocess.PIPE)
dmenu_command = str(output.stdout.decode("utf-8")).strip('\n')
# TODO: don't wait for process
subprocess.run(dmenu_command.split('\n')[-1].strip('\t'), shell=True, stdout=None, stdin=None, stderr=None, close_fds=True)  # don't wait for it
