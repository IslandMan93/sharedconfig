# from source
sudo apt install -y sxhkd
sudo apt-get install -y xcb libxcb-shape0-dev libxcb-util0-dev libxcb-ewmh-dev libxcb-randr0-dev libxcb-icccm4-dev libxcb-keysyms1-dev libxcb-xinerama0-dev libasound2-dev libxcb-xtest0-dev
cd /tmp && git clone https://github.com/baskerville/bspwm.git
cd bspwm
make
sudo make install

# copy into xsessions
sudo cp contrib/freedesktop/bspwm.desktop /usr/share/xsessions/

# link configs
mkdir ~/.config/bspwm
mkdir ~/.config/sxhkd
ln -s ~/sharedconfig/twm/bspwm/bspwmrc ~/.config/bspwm/bspwmrc
ln -s ~/sharedconfig/twm/bspwm/sxhkdrc ~/.config/sxhkd/sxhkdrc

# helper scripts
sudo chmod +x ~/sharedconfig/twm/bspwm/open_or_create.sh
sudo chmod +x ~/sharedconfig/twm/bspwm/reorder_or_remove_desktops.sh
sudo chmod +x ~/sharedconfig/twm/bspwm/swap_monitor.sh
sudo chmod +x ~/sharedconfig/twm/bspwm/rename.sh
sudo chmod +x ~/sharedconfig/twm/bspwm/create_and_move.sh

cd ~
