#!/bin/bash

# matches desktop based on the first character in the friendly name
desktop_name_match () {
    array_index=0
    for d in $(bspc query -D --names); do
        if [ "${d:0:1}" -eq "$1" ]; then
            echo "$array_index"
            return 1
        fi
        array_index=$((1 + $array_index))
    done
    echo ""
}
match_index=$(desktop_name_match $1)
desktop_ids=($(bspc query -D))
if [ -n "$match_index" ]; then
    # desktop exists focus it
    bspc desktop -f "${desktop_ids[$match_index]}"
else
    # desktop doesn't exist create and focus
    current_monitor=$(bspc query -M -d $(bspc query -D -d focused))
    bspc monitor $current_monitor --add-desktops $1
    bspc desktop -f $1
fi

# twm reorder or remove desktops
~/sharedconfig/twm/bspwm/reorder_or_remove_desktops.sh
