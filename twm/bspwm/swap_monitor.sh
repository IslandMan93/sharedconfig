#!/bin/bash

current_desktop=$(bspc query -D -d focused)
current_monitor=$(bspc query -M -d $current_desktop)
# all desktops are on a new line so use wc
num_desktops=$(bspc query -D -m $current_monitor | wc -l)

# have to create a desktop
if [ "$num_desktops" -lt "2" ]; then
    possible=1
    # next available workspace
    current_desktops=$(bspc query -D --names | sort -n)
    for d in $current_desktops; do
        echo "$possible $d"
        if [ "$possible" != "$d" ]; then
            break
        else
            possible=$(($possible+1))
        fi
    done
    bspc monitor $current_monitor --add-desktops $possible
fi

# move it
bspc desktop $current_desktop -m next
bspc desktop $current_desktop -f

# twm reorder or remove desktops
~/sharedconfig/twm/bspwm/reorder_or_remove_desktops.sh
