#!/bin/bash

# remove unused desktops like i3
for desktop_name in $(bspc query -D); do
    windows=$(bspc query -N -d $desktop_name)
    # if no windows and not focused remove desktop
    if [ -z "$windows" ] && [ $(bspc query -D -d) != "$desktop_name" ]; then
        bspc desktop $desktop_name -r
    fi
done

# monitor list
for monitor_name in $(bspc query -M); do
    # get desktop pretty names sorted
    sorted_names=$(bspc query -D -m $monitor_name --names | sort -n)
    # echo "$monitor_name $sorted_names"
    bspc monitor $monitor_name -o $sorted_names
done