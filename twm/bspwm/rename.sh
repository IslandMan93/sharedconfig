#!/bin/bash

current_desktop=$(bspc query -D -d focused --names)

# rename
entered_name=$(rofi -dmenu -p "Rename $current_desktop" -lines 0)
if [ -n "$entered_name" ]; then
    new_name="${current_desktop:0:1}.$entered_name"
    bspc desktop focused -n "$new_name"
fi