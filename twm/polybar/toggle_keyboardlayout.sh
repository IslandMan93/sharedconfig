#!/bin/bash

variant=$(setxkbmap -query | grep variant)
if [ -z "$variant" ]; then
    setxkbmap -layout us -variant colemak
else
    setxkbmap -layout us
fi
