#! /usr/bin/env python3

import sys
import dbus
import argparse


parser = argparse.ArgumentParser()
parser.add_argument(
    '-t',
    '--trunclen',
    type=int,
    metavar='trunclen'
)
parser.add_argument(
    '-f',
    '--format',
    type=str,
    metavar='custom format',
    dest='custom_format'
)
parser.add_argument(
    '-p',
    '--playpause',
    type=str,
    metavar='play-pause indicator',
    dest='play_pause'
)
parser.add_argument(
    '-c',
    '--click',
    type=int,
    default=0
)

args = parser.parse_args()

# Default parameters
COLOR_SPOTIFY, COLOR_PITHOS = '#81b71a', '#00A0EE'
output = '{play_pause} {artist}: {song} %{{F-}}'
trunclen = 25
PLAY_PAUSE_ICONS = u'\uf04b,\uf04c' # first character is play, second is paused

# parameters can be overwritten by args
if args.trunclen is not None:
    trunclen = args.trunclen
if args.custom_format is not None:
    output = args.custom_format
if args.play_pause is not None:
    PLAY_PAUSE_ICONS = args.play_pause

def status(dbus_address, color='#FFFFFF'):
    session_bus = dbus.SessionBus()
    spotify_bus = session_bus.get_object(
        dbus_address,
        '/org/mpris/MediaPlayer2'
    )

    spotify_properties = dbus.Interface(
        spotify_bus,
        'org.freedesktop.DBus.Properties'
    )

    # handle clicks
    if args.click > 0:
        control_iface = dbus.Interface(spotify_bus, 'org.mpris.MediaPlayer2.Player')
        if (args.click > 0 and args.click < 3):
            control_iface.PlayPause()
        elif (args.click == 5):
            control_iface.Next()
        elif (args.click == 4):
            control_iface.Previous()

    metadata = spotify_properties.Get('org.mpris.MediaPlayer2.Player', 'Metadata')
    status = spotify_properties.Get('org.mpris.MediaPlayer2.Player', 'PlaybackStatus')

    play_pause = PLAY_PAUSE_ICONS.split(',')
    if status == 'Playing':
        play_pause = play_pause[0]
    elif status == 'Paused':
        play_pause = play_pause[1]
    elif status == 'Stopped':
        print('')
        exit()
    else:  # unknown status
        play_pause = str()

    artist = metadata['xesam:artist'][0]
    song = metadata['xesam:title']

    if len(song) > trunclen:
        song = song[0:trunclen]
        song += '...'
        if ('(' in song) and (')' not in song):
            song += ')'

    # Python3 uses UTF-8 by default.
    if sys.version_info.major == 3:
        print('%{{F{}}}'.format(color) + output.format(artist=artist, song=song, play_pause=play_pause))
    else:
        print('%{{F{}}}'.format(color).encode('UTF-8') + output.format(artist=artist, song=song, play_pause=play_pause).encode('UTF-8'))

try:
    status('org.mpris.MediaPlayer2.spotify', color=COLOR_SPOTIFY)
except Exception as e:
    if isinstance(e, dbus.exceptions.DBusException):
        # app isn't open, try pithos
        try:
            status('org.mpris.MediaPlayer2.pithos', color=COLOR_PITHOS)
        except Exception as e:
            # pithos not open either
            if isinstance(e, dbus.exceptions.DBusException):
                print('')
            else:
                print('')  # pithos has a dbus error inbetween songs just don't show anything
    else:
        print('')  # pithos has a dbus error inbetween songs just don't show anything
