#! /usr/bin/env python3
'''
This program is designed to work in tandem with dunst and i3blocks to extend
the functionality of the existing i3blocks notification blocklet and allow
multiple notifications of different priority levels to stack and be displayed.
Copyright (C) 2018 IslandMan93 Copyright (C) 2014  Tomasz Kramkowski

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'''

import os
import sys
import json
import subprocess

path = os.environ["HOME"] + "/.cache/i3blocks-dunst/notifications"
strlimit = 80
SILENCED_APPS = ['Spotify', 'pithos']
DMENU_COMMANDS = ['d:    Delete Top', 'dg:   Delete All', 'd<n>: Delete Number <n>']

def full_text(notification):
    if len(notification["summary"] + notification["body"]) + 1 > strlimit:
        return "[{}] {}".format(notification["summary"],
                                notification["body"])[:strlimit - 3] + "..."
    else:
        return "[{}] {}".format(notification["summary"], notification["body"])

def short_text(notification):
    if len(notification["summary"]) > strlimit:
        return notification["summary"][:strlimit - 3] + "..."
    else:
        return notification["summary"]

def color(notification):
    if notification["urgency"] == "LOW":
        return "#FFFFFF"
    elif notification["urgency"] == "NORMAL":
        return "#00FF00"
    elif notification["urgency"] == "CRITICAL":
        return "#FF0000"
    else:
        return "#FFFFFF"

def load_notifications():
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    if not os.path.isfile(path):
        f = open(path, "w+")
    else:
        f = open(path, "r")
    try:
        loaded = json.load(f)
        current_index = int(loaded['current_index'])
        notifications = loaded['notifications']
    except:
        current_index = -1
        notifications = list()
    f.close()
    return notifications, current_index

def save_notifications(notifications, current_index=-1):
    f = open(path, "w")
    json.dump({'current_index': current_index, 'notifications': notifications}, f)
    f.close()

def save_index(notifications, current_index):
    f = open(path, "w")
    json.dump({'current_index': current_index, 'notifications': notifications}, f)
    f.close()

def calledby_menu():
    return (len(sys.argv) == 2 and 'menu' in sys.argv[1])

def calledby_status():
    return (len(sys.argv) == 2 and 'status' in sys.argv[1])

def gettop(current_index):
    last = current_index
    for k, v in enumerate(notifications):
        crit = False
        if v["urgency"] == "CRITICAL":
            last = k
            crit = True
        if crit: return last
    for k, v in enumerate(notifications):
        norm = False
        if v["urgency"] == "NORMAL":
            last = k
        if norm: return last
    for k, v in enumerate(notifications):
        low = False
        if v["urgency"] == "LOW":
            last = k
        if low: return last
    return current_index

def increment_index(current_index, notifications):
    current_index -= 1
    if abs(current_index) > len(notifications):
        current_index = -len(notifications)
    save_index(notifications, current_index)

def decrement_index(current_index, notifications):
    current_index += 1
    if current_index > -1:
        current_index = -1
    save_index(notifications, current_index)

def process_dmenu_command(cmd, current_index, notifications):
    # vim style commands
    if len(cmd) > 0:
        if cmd[0:2] == 'dg':
            notifications = []
            current_index = -1
        elif cmd[0:2] == 'd:':  # here just make sure it's the d command by checking for d:
            current_index = -1
            notifications.pop(0)
        elif cmd[0] == 'd' and cmd[1:].isdigit():
            current_index = -1
            for i in range(int(cmd[1:])):
                if len(notifications) > 1:
                    notifications.pop(0)
        else:  # assuming that it's an app
            # TODO: try to focus it?
            pass
    save_notifications(notifications, current_index)


if __name__ == "__main__":
    notifications, current_index = load_notifications()
    if calledby_status():
        if sys.argv[1] == 'status:click':
            if len(notifications) > 0:
                notifications.pop(gettop(current_index))
                save_notifications(notifications)
        elif sys.argv[1] == 'status:right-click':
            current_index = -1
            save_index(notifications, current_index)
        elif sys.argv[1] == 'status:next':
            increment_index(current_index, notifications)
        elif sys.argv[1] == 'status:prev':
            decrement_index(current_index, notifications)

        if len(notifications) <= 0:
            print("")
            print("")
            print("")
            sys.exit(0)

        top = gettop(current_index)

        amount = ""
        if len(notifications) > 1:
            amount = " ({}/{})".format(len(notifications) + current_index + 1, len(notifications))
        print(full_text(notifications[top]) + amount)
        print(short_text(notifications[top]) + amount)
        print(color(notifications[top]))
        sys.exit(0)
    elif calledby_menu():
        str_to_rofi = '\n'.join(DMENU_COMMANDS) + '\n'
        for n_ind, n in enumerate(notifications):
            str_to_rofi += '{}. {}: {}. {}\n'.format(n_ind, n['app'], n['summary'], n['body'])
        # fix some escape chars
        str_to_rofi = str_to_rofi.replace("'", "'\\''")
        print(str_to_rofi)
        output = subprocess.run("echo '{}' | rofi -dmenu".format(str_to_rofi), shell=True, stdout=subprocess.PIPE)
        dmenu_command = str(output.stdout.decode("utf-8")).strip('\n')
        process_dmenu_command(dmenu_command, current_index, notifications)

    else:
        if sys.argv[1] not in SILENCED_APPS:
            notifications.append({
                                    "app"       : sys.argv[1],
                                    "summary"   : sys.argv[2],
                                    "body"      : sys.argv[3],
                                    "icon"      : sys.argv[4],
                                    "urgency"   : sys.argv[5]
                                })
            save_notifications(notifications)
        sys.exit(0)

