sudo apt install -y xcb-proto libxcb-ewmh-dev python-xcbgen libnl-3-dev libcairo2-dev libxcb-xkb-dev
sudo apt install -y libxcb-composite0-dev libxcb1-dev libxcb-util0-dev libxcb-randr0-dev libxcb-image0-dev libxcb-icccm4-dev
# network pulseaudio and curl
sudo apt install -y pulseaudio libpulse-dev libnl-genl-3-dev
# you need git & cmake if not installed
sudo apt install -y git cmake
cd /tmp
git clone --branch 3.4 --recursive https://github.com/jaagr/polybar
mkdir polybar/build
cd polybar/build
cmake ..
sudo make install

# link config
mkdir ~/.config/polybar
ln -s ~/sharedconfig/twm/polybar/config ~/.config/polybar/config

sudo chmod +x ~/sharedconfig/twm/polybar/launch.sh
cd ~

# spotify control
sudo apt install -y python-pip libdbus-glib-1-dev
pip install dbus-python
