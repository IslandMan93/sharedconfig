#!/bin/bash

# window manager stuff
bash dunst/install.sh
bash rofi/install.sh
bash polybar/install.sh
# may want to just test that polybar works
#bash polybar/launch.sh

bash compton/install.sh
# finally the actual window manager
bash bspwm/install.sh

cd ~
echo "Highly recommended to also run install_opt.sh"

# make chrome use system title bar and borders under settings
