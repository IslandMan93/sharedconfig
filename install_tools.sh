### Random tools install script

### TOOLS
## nvim
bash nvim/install.sh

## urxvt
bash urxvt/install.sh

## zsh
sudo apt install -y ncurses-dev autoconf
cd /tmp
git clone git://git.code.sf.net/p/zsh/code zsh
cd zsh
./Util/preconfig
./configure
sudo make install
cd ~
## oh my zsh
sh -c "$(wget -O- https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# remove omz default config
rm ~/.zshrc
ln -s ~/sharedconfig/.zshrc ~/.zshrc

# zsh theme
git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/themes/powerlevel9k

## fzf
sudo apt install -y fzf

## tmux install
sudo apt install -y libevent-dev
cd /tmp
wget https://github.com/tmux/tmux/releases/download/2.9a/tmux-2.9a.tar.gz
tar -xvf tmux-2.9a.tar.gz
cd tmux-2.9a
./configure && make
sudo make install
cd ~
# tmux powerline theme
git clone https://github.com/jimeh/tmux-themepack.git ~/.tmux-themepack
ln -s ~/sharedconfig/.tmux.conf ~/.tmux.conf
ln -s ~/sharedconfig/.tmux.conf.local ~/.tmux.conf.local
